const responsiveWidth = 991;
const windowWidth = $(window).width();
const burger = $('.burger');
const nav = $('.nav-list');

$(document).ready(function () {

    // TABLE MOVE BACKGROUND ON HOVER //
    $('.table-wrap tr, .faq-list-item, .nav-list a').on('mousemove', function () {
        let row = $(this);
        let table = row.closest('.table-wrap');
        let bg = table.find('.table-wrap-bg');
        let rowTop = row.position().top;
        let rowHeight = row.outerHeight();
        bg.css({
            'height': ( rowHeight + 4 ),
            'top': ( rowTop - 2 )
        });
    });
    $('.table-wrap').on('mouseleave', function () {
        let bg = $(this).find('.table-wrap-bg');
        bg.css({
            'height': 0
        });
    });

    // SET NUMBERS FOR LIST ITEMS //
    $('.features-list-item').each(function (i) {
        i += 1;
        $(this).attr('data-num', i <= 9 ? '0' + i : i);
    });

    // TOGGLE MAIN MENU //
    $(document).on('click', '.burger', function () {
        $(this).hasClass('__open-theme') ? closeMenu() : openMenu();
    });

    $('[data-fancybox]').fancybox({
        buttons: [
            'close'
        ]
    });

    // TABS SWITCH //
    $(document).on('click', '.tabs-link', function () {

        let btn = $(this);
        let btnId = btn.attr('data-tab-link-id');
        let tab = $('.tabs-content-item[data-tab-content-id="'+ btnId +'"]');

        $('.tabs-content-item').not(tab).hide();
        $('.tabs-link').not(btn).removeClass('active');

        btn.addClass('active');
        tab.show();
    });

    $('.input-item[type="file"]').on('change', function (e) {
        let fileName = e.target.files[0].name;
        $(this).parent('.input-container').find('.input-item-title').html(fileName);
    });

    $('.form-wrap.__feedback form').on('submit', function (e) {

        if(!isEmptyFields($(this))) {

        } else {
            e.preventDefault();
        }

    });

    $('.drop-words').each(function () {
        let wordWrap = $(this);
        let word = wordWrap.text();
        let re = /\s* \s*/;
        let newStr = word.split(re);
        wordWrap.html('');
        $.each(newStr, function () {
            let newWord = this;
            wordWrap.append('<span class="word">'+ newWord +'</span> ');
        });
    });

    // // TEXTAREA ROWS DETECT //
    const textarea = document.querySelector('#message');
    if(textarea) {
        textarea.addEventListener('keydown', autosize);

        function autosize() {
            let el = this;
            setTimeout(function () {
                el.style.cssText = 'height:auto; padding:0';
                // for box-sizing other than "content-box" use:
                // el.style.cssText = '-moz-box-sizing:content-box';
                el.style.cssText = 'height:' + el.scrollHeight + 'px';
            }, 0);
        }
    }

    // REPLACE DESKTOP TITLE TO MOBILE //
    replaceFolderTitle();

});

function openMenu() {
    nav.fadeIn(400);
    burger.addClass('__open-theme');
    setTimeout(function () {
        nav.addClass('__open-theme');
    }, 400);
}

function closeMenu() {
    nav.removeClass('__open-theme');
    burger.removeClass('__open-theme');
    setTimeout(function () {
        nav.fadeOut(400);
    }, 400);
}

function replaceFolderTitle() {
    if(windowWidth <= responsiveWidth) {
        let folderTitle = $('.folder-content-title');
        let folderSubTitle = $('.folder-content-subtitle');
        let pageTitle = $('.section-title.__main-theme');
        let navContent = $('.nav-content-title');
        if(folderSubTitle[0]){
            navContent.append('<h2 class="title-2 folder-content-subtitle">' + folderSubTitle.text() + '</h2>');
        }
        if(folderTitle[0]) {
            navContent.append('<h1 class="title-1 folder-content-title">' + folderTitle.text() + '</h1>');
        }
        if(pageTitle[0]){
            navContent.append('<h1 class="title-1 folder-content-title">' + pageTitle.text() + '</h1>');
        }
    }
}

function isEmptyFields(form) {

    let status = true;

    form.find('[data-required]').each(function () {

        let input = $(this);

        if(input.val().length <= 0) {

            input.closest('.row').addClass('__is-empty');

        } else if(input.val().length >= 0) {

            input.closest('.row').removeClass('__is-empty');

        }

    });

    let container = form.find('.row.__is-empty');

    container[0] ? status = true : status = false;

    return status;

}